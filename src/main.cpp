/***************************************************************************
 *   Copyright (C) %{CURRENT_YEAR} by Lindsay Mathieson <lindsay.mathieson@kdemail.net>                            *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
 ***************************************************************************/

#include "kwhirlmon.h"
#include <KDE/KApplication>
#include <KDE/KUniqueApplication>
#include <KDE/KAboutData>
#include <KDE/KCmdLineArgs>
#include <KDE/KLocale>
#include "settings.h"

static const char description[] =
    I18N_NOOP("A KDE 4 for monitoring Whirlpool watched threads");

static const char version[] = "1.0";

int main(int argc, char **argv)
{
    KAboutData about("kwhirlmon", 0, ki18n("KWhirlMon"), version, ki18n(description),
                     KAboutData::License_GPL, ki18n("(C) 2012 Lindsay Mathieson"), KLocalizedString(), 0, "Lindsay Mathieson <lindsay.mathieson@kdemail.net>");
    about.addAuthor( ki18n("Lindsay Mathieson"), KLocalizedString(), "Lindsay Mathieson <lindsay.mathieson@kdemail.net>" );
    KCmdLineArgs::init(argc, argv, &about);

    KCmdLineOptions options;
    options.add("+[URL]", ki18n( "Document to open" ));
    KCmdLineArgs::addCmdLineOptions(options);
    KUniqueApplication app;
    //KApplication app;

    QCoreApplication::setOrganizationName("whirlmon");
    QCoreApplication::setOrganizationDomain("whirlmon");
    QCoreApplication::setApplicationName("whirlmon");


    // see if we are starting with session management
    // We have no need for session management
	/*
    if (app.isSessionRestored())
    {
        RESTORE(KWhirlMon);
    }
    else
	*/
    {
        // no session.. just start up normally
        KWhirlMon *widget = new KWhirlMon;

        if (! Settings::start_in_tray())
            widget->show();
    }

    return app.exec();
}
