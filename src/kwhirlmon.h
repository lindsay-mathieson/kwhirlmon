/***************************************************************************
 *   Copyright (C) %{CURRENT_YEAR} by Lindsay Mathieson <lindsay.mathieson@kdemail.net>                            *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
 ***************************************************************************/

#ifndef KWHIRLMON_H
#define KWHIRLMON_H


#include <KXmlGuiWindow>
#include <KAction>
#include <KStatusNotifierItem>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QTimer>


#include "ui_prefs_base.h"

class KWhirlMonView;
class QPrinter;
class KToggleAction;
class KUrl;

/**
 * This class serves as the main window for KWhirlMon.  It handles the
 * menus, toolbars and status bars.
 *
 * @short Main window class
 * @author Lindsay Mathieson <lindsay.mathieson@kdemail.net>
 * @version %{VERSION}
 */
class KWhirlMon : public KXmlGuiWindow
{
    Q_OBJECT
public:
    /**
     * Default Constructor
     */
    KWhirlMon();

    /**
     * Default Destructor
     */
    virtual ~KWhirlMon();
	KAction *aRefresh;
	KAction *aMarkRead;
	KAction *aMarkAllRead;

private:
	QNetworkAccessManager manager;
	QTimer m_RefreshTimer;
	
protected:
	 void closeEvent(QCloseEvent *event);

private slots:
	void refresh();
	void refreshFinished();
	void markReadFinished();
	void tmRefresh();
	void updateCount();
    void optionsPreferences();
	void settingsChanged();
	void onQuit();
	void threadActivated(QModelIndex cell);
	void notify();
	void markRead();
	void markAllRead();
	void customContextMenuRequested(const QPoint & pos);
	void selectionChanged (const QItemSelection & selected, const QItemSelection & deselected);


private:
    void setupActions();
	void MarkThreadRead(int row, bool autoHide);

private:
    Ui::prefs_base ui_prefs_base ;
    KWhirlMonView *m_view;
	KStatusNotifierItem *m_Notifier;
};

#endif // _KWHIRLMON_H_
