/***************************************************************************
*   Copyright (C) %{CURRENT_YEAR} by Lindsay Mathieson <lindsay.mathieson@kdemail.net>                            *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
***************************************************************************/

#include "kwhirlmonview.h"
#include "settings.h"

#include <KLocale>
#include <KAction>
#include <QtGui/QLabel>
#include <QAbstractTableModel>
#include <qjson/parser.h>
#include <QSettings>



WatchedThreadModel::WatchedThreadModel(QObject *parent) :
QAbstractTableModel(parent)
{
}

int WatchedThreadModel::rowCount(const QModelIndex &parent) const
{
	Q_UNUSED(parent);

	return threads.count();
}

int WatchedThreadModel::columnCount(const QModelIndex &parent) const
{
	Q_UNUSED(parent);

	return 4;
}



QVariant WatchedThreadModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (role == Qt::DisplayRole)
	{
		if (orientation == Qt::Horizontal) {
			switch (section)
			{
				case 0:
					return QString("Title");
				case 1:
					return QString("Unread");
				case 2:
					return QString("Last Poster");
				case 3:
					return QString("Date");
			}
		}
	}
	else if (role == Qt::TextAlignmentRole)
	{
		switch (section)
		{
			case 1: return Qt::AlignRight;
		}
		return Qt::AlignLeft;
	}
	return QVariant();
}


QVariant WatchedThreadModel::data(const QModelIndex &index, int role) const
{
	const WatchedThreadData &th = threads[index.row()];
	if (role == Qt::DisplayRole)
	{
		{
			switch (index.column())
			{
				case 0: return th.title;
				case 1: return th.unread;
				case 2: return th.lastName;
				case 3: return th.lastDate;
			}
		}
	}
	else if (role == Qt::FontRole)
	{
		if (th.readMarkPending)
		{
			QFont f;
			f.setItalic(true);
			f.setStrikeOut(true);
			return f;
		}

		if (! Settings::unread_only() && th.unread > 0)
		{
			QFont f;
			f.setBold(true);
			return f;
		}
	}
	else if (role == Qt::ForegroundRole)
	{
		if (! Settings::unread_only() && th.unread > 0)
		{
			return QBrush(QColor("red"));
		}
	}
	else if (role == Qt::TextAlignmentRole)
	{
		switch (index.column())
		{
			case 1: return (Qt::AlignRight + Qt::AlignVCenter);
		}
		return Qt::AlignLeft + Qt::AlignVCenter;
	}
	return QVariant();
}

bool WatchedThreadModel::removeRows(int row, int count, const QModelIndex& parent)
{
	beginRemoveRows(parent, row, row + count - 1);
	threads.remove(row, count);
	endRemoveRows();
	return true;
}

bool lessThan( const WatchedThreadData & e1, const WatchedThreadData & e2 )
{
    return e1.unread > e2.unread;
}

void WatchedThreadModel::setThread(const WatchedThreadData::List& qthreads)
{
	threads = qthreads;
	if (! Settings::unread_only())
	{
			qSort(threads.begin(), threads.end(), lessThan);
	}
	emit reset();
}

void WatchedThreadModel::setPendingMarkRead(int row, bool setTo)
{
	QModelIndex index = createIndex(row, 0);
	threads[index.row()].readMarkPending = setTo;
	emit dataChanged(index, index);
}



KWhirlMonView::KWhirlMonView(QWidget *) : threadModel(NULL)
{
	setEditTriggers(QAbstractItemView::NoEditTriggers);
	setProperty("showDropIndicator", QVariant(false));
	setAlternatingRowColors(true);
	setSelectionMode(QAbstractItemView::SingleSelection);
	setSelectionBehavior(QAbstractItemView::SelectRows);
	// setSortingEnabled(true);
	setItemsExpandable(false);
	setRootIsDecorated(false);
	setContextMenuPolicy(Qt::CustomContextMenu);
	
	QSettings settings;
	QByteArray state = settings.value("watchedCols").toByteArray();
	if (!state.isEmpty())
		header()->restoreState(state);
	

	
	setAutoFillBackground(true);
	setModel(&threadModel);

}

KWhirlMonView::~KWhirlMonView()
{
	QByteArray state = header()->saveState();
	QSettings settings;
	settings.setValue("watchedCols", state);
}


#include "kwhirlmonview.moc"
