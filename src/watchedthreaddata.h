/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  Lindsay Mathieson <lindsay.mathieson@kdemail.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#ifndef WATCHEDTHREADDATA_H
#define WATCHEDTHREADDATA_H

#include <QString>
#include <QVector>
#include <QDateTime>
#include <QUrl>


class WatchedThreadData
{
public:
	long 	id;
	QString title;
	int 	replies;
	int 	unread;
	int 	lastRead;
	int 	lastPage;
    long    lastId;
	QString 	lastName;	
	QDateTime 	lastDate;

	// State
	bool readMarkPending;

	inline WatchedThreadData() : readMarkPending(false) {};

	class Summary
	{
	public:
		inline Summary() : nUnreadMsgs(0), nUnreadThreads(0) {}
		int nUnreadMsgs;
		int nUnreadThreads;

		QString getText();
	};
	
	class List : public QVector<WatchedThreadData>
	{
	public:
		Summary getSummary();
	};

	static List parseJson(QByteArray& json);

	static QUrl getWatchedUrl(bool unreadOnly);

	QUrl getOpenUrl() const;
	QUrl getMarkReadUrl(bool autoHide) const;

	static bool NewMessagesInW2(const List& w1, const List& w2);	
};

#endif // WATCHEDTHREADDATA_H
