/***************************************************************************
*   Copyright (C) %{CURRENT_YEAR} by Lindsay Mathieson <lindsay.mathieson@kdemail.net>                            *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
***************************************************************************/

#ifndef KWHIRLMONVIEW_H
#define KWHIRLMONVIEW_H

#include <QtGui/QWidget>
#include <QTreeView>
#include <QHeaderView>

#include "watchedthreaddata.h"

class QPainter;
class KUrl;

class WatchedThreadModel : public QAbstractTableModel
{
	Q_OBJECT
public:
	WatchedThreadData::List threads;

	WatchedThreadModel(QObject *parent);
	int rowCount(const QModelIndex &parent = QModelIndex()) const ;
	int columnCount(const QModelIndex &parent = QModelIndex()) const;

	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    bool removeRows(int row, int count, const QModelIndex& parent = QModelIndex());

	void setThread(const WatchedThreadData::List& qthreads);
    void setPendingMarkRead(int row, bool setTo);
};


/**
* This is the main view class for KWhirlMon.  Most of the non-menu,
* non-toolbar, and non-statusbar (e.g., non frame) GUI code should go
* here.
*
* @short Main view
* @author Lindsay Mathieson <%{EMAIL}>
* @version %{VERSION}
*/

class KWhirlMonView : public QTreeView
{
	Q_OBJECT
public:
	/**
	* Default constructor
	*/
	KWhirlMonView(QWidget *parent);

	/**
	* Destructor
	*/
	virtual ~KWhirlMonView();
	WatchedThreadModel	threadModel;


signals:
	/**
	* Use this signal to change the content of the statusbar
	*/
	void signalChangeStatusbar(const QString& text);

	/**
	* Use this signal to change the content of the caption
	*/
	void signalChangeCaption(const QString& text);

};

#endif // KWhirlMonVIEW_H
